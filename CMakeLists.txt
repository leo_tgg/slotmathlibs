cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(slotRuleLib)

set(CMAKE_CXX_COMPILER g++)
add_compile_options(-std=c++11 -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-qual -Winline
	            -Wunreachable-code -fPIC -D_REENTRANT
		    )

SET(CMAKE_BUILD_TYPE Debug)

# java include dir
include_directories(/usr/opt/javatools/jdk1.7.0_79/include
	/usr/opt/javatools/jdk1.7.0_79/include/linux)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/rapidjson)


set(SRCDIR ${CMAKE_CURRENT_SOURCE_DIR}/main)
set(SOURCES
	${SRCDIR}/SlotUtility.cpp
	${SRCDIR}/SlotRuleLib.cpp
	${SRCDIR}/dataSerialize.cpp
	${SRCDIR}/TGGGameMathWebUtils.cpp
	CACHE INTERNAL "" FORCE
	)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/games)


add_library(TGGGameMathLib SHARED ${SOURCES})
#message(STATUS "source files = ${SOURCES}")
add_executable(test ${SOURCES} ${SRCDIR}/Test.cpp)

set_target_properties(TGGGameMathLib PROPERTIES VERSION 1.0)


/******************************************************************************\
 * File    : slotCarding.h
 * Purpose :
\******************************************************************************/

#ifndef _slot_game_personal_carding_h_
#define _slot_game_personal_carding_h_
#include "RollingGoldSymbolDefine.hpp"
#include "RollingGoldData.hpp"
namespace rollingGold {
        static const uint32_t reelStops_N1[MAX_NUM_REELS] = {88,88,88,88,88};
        static const uint32_t reelStops_F1[MAX_NUM_REELS] = {65,65,65,65,65};
        static const uint8_t reelCarding_N1[MAX_NUM_REELS][MAX_NUM_STOPS] = {
                {
                        SYM_N,SYM_J,SYM_GF,SYM_ZB,SYM_K,
                        SYM_A,SYM_Q,SYM_T,SYM_J,SYM_N,
                        SYM_J,SYM_ZB,SYM_N,SYM_T,SYM_FE,
                        SYM_T,SYM_J,SYM_GF,SYM_J,SYM_TR,
                        SYM_LN,SYM_N,SYM_TR,SYM_LN,SYM_K,
                        SYM_N,SYM_TR,SYM_Q,SYM_T,SYM_J,
                        SYM_SCAT,SYM_J,SYM_A,SYM_K,SYM_SCAT,
                        SYM_N,SYM_T,SYM_N,SYM_LN,SYM_LN,
                        SYM_LN,SYM_LN,SYM_ZB,SYM_Q,SYM_FE,
                        SYM_LN,SYM_K,SYM_K,SYM_Q,SYM_A,
                        SYM_ZB,SYM_Q,SYM_T,SYM_Q,SYM_FE,
                        SYM_T,SYM_GF,SYM_A,SYM_FE,SYM_TR,
                        SYM_SCAT,SYM_GF,SYM_LN,SYM_ZB,SYM_TR,
                        SYM_K,SYM_TR,SYM_T,SYM_A,SYM_Q,
                        SYM_T,SYM_TR,SYM_N,SYM_ZB,SYM_FE,
                        SYM_Q,SYM_N,SYM_K,SYM_ZB,SYM_GF,
                        SYM_A,SYM_J,SYM_Q,SYM_TR,SYM_N,
                        SYM_FE,SYM_GF,SYM_T,
                },
                {
                        SYM_A,SYM_TR,SYM_Q,SYM_N,SYM_GF,
                        SYM_LN,SYM_LN,SYM_LN,SYM_LN,SYM_ZB,
                        SYM_A,SYM_J,SYM_N,SYM_Q,SYM_SCAT,
                        SYM_Q,SYM_T,SYM_FE,SYM_J,SYM_K,
                        SYM_N,SYM_FE,SYM_TR,SYM_K,SYM_LN,
                        SYM_FE,SYM_K,SYM_FE,SYM_GF,SYM_LN,
                        SYM_J,SYM_Q,SYM_J,SYM_N,SYM_LN,
                        SYM_J,SYM_ZB,SYM_T,SYM_SCAT,SYM_T,
                        SYM_TR,SYM_T,SYM_A,SYM_SCAT,SYM_J,
                        SYM_T,SYM_N,SYM_TR,SYM_Q,SYM_A,
                        SYM_K,SYM_Q,SYM_FE,SYM_T,SYM_J,
                        SYM_TR,SYM_ZB,SYM_K,SYM_Q,SYM_T,
                        SYM_N,SYM_FE,SYM_K,SYM_N,SYM_LN,
                        SYM_SCAT,SYM_N,SYM_ZB,SYM_N,SYM_TR,
                        SYM_T,SYM_TR,SYM_GF,SYM_WILD,SYM_WILD,
                        SYM_WILD,SYM_WILD,SYM_A,SYM_ZB,SYM_A,
                        SYM_ZB,SYM_J,SYM_LN,SYM_Q,SYM_T,
                        SYM_WILD,SYM_GF,SYM_Q,
                },
                {
                        SYM_LN,SYM_LN,SYM_LN,SYM_LN,SYM_FE,
                        SYM_TR,SYM_K,SYM_GF,SYM_J,SYM_GF,
                        SYM_LN,SYM_FE,SYM_A,SYM_K,SYM_J,
                        SYM_LN,SYM_TR,SYM_SCAT,SYM_GF,SYM_J,
                        SYM_N,SYM_TR,SYM_J,SYM_SCAT,SYM_T,
                        SYM_Q,SYM_N,SYM_K,SYM_J,SYM_T,
                        SYM_ZB,SYM_T,SYM_N,SYM_K,SYM_A,
                        SYM_GF,SYM_Q,SYM_K,SYM_N,SYM_Q,
                        SYM_FE,SYM_J,SYM_ZB,SYM_TR,SYM_K,
                        SYM_TR,SYM_N,SYM_T,SYM_FE,SYM_SCAT,
                        SYM_J,SYM_A,SYM_Q,SYM_K,SYM_TR,
                        SYM_A,SYM_T,SYM_Q,SYM_ZB,SYM_J,
                        SYM_A,SYM_Q,SYM_TR,SYM_SCAT,SYM_FE,
                        SYM_T,SYM_ZB,SYM_T,SYM_J,SYM_N,
                        SYM_LN,SYM_TR,SYM_WILD,SYM_N,SYM_T,
                        SYM_TR,SYM_GF,SYM_ZB,SYM_N,SYM_SCAT,
                        SYM_ZB,SYM_A,SYM_Q,SYM_GF,SYM_N,
                        SYM_T,SYM_FE,SYM_ZB,
                },
                {
                        SYM_FE,SYM_T,SYM_FE,SYM_J,SYM_TR,
                        SYM_N,SYM_ZB,SYM_WILD,SYM_Q,SYM_ZB,
                        SYM_J,SYM_Q,SYM_J,SYM_FE,SYM_N,
                        SYM_T,SYM_TR,SYM_T,SYM_Q,SYM_K,
                        SYM_TR,SYM_N,SYM_TR,SYM_K,SYM_N,
                        SYM_J,SYM_GF,SYM_ZB,SYM_Q,SYM_T,
                        SYM_A,SYM_Q,SYM_J,SYM_N,SYM_ZB,
                        SYM_N,SYM_A,SYM_T,SYM_J,SYM_ZB,
                        SYM_J,SYM_N,SYM_J,SYM_T,SYM_LN,
                        SYM_LN,SYM_LN,SYM_LN,SYM_J,SYM_T,
                        SYM_A,SYM_TR,SYM_Q,SYM_GF,SYM_A,
                        SYM_WILD,SYM_GF,SYM_Q,SYM_LN,SYM_FE,
                        SYM_T,SYM_FE,SYM_K,SYM_ZB,SYM_K,
                        SYM_ZB,SYM_J,SYM_N,SYM_A,SYM_Q,
                        SYM_A,SYM_K,SYM_LN,SYM_Q,SYM_ZB,
                        SYM_LN,SYM_K,SYM_TR,SYM_K,SYM_GF,
                        SYM_T,SYM_T,SYM_N,SYM_N,SYM_FE,
                        SYM_A,SYM_A,SYM_FE,
                },
                {
                        SYM_T,SYM_K,SYM_FE,SYM_ZB,SYM_T,
                        SYM_J,SYM_Q,SYM_A,SYM_ZB,SYM_K,
                        SYM_J,SYM_TR,SYM_A,SYM_ZB,SYM_N,
                        SYM_FE,SYM_T,SYM_N,SYM_A,SYM_TR,
                        SYM_LN,SYM_LN,SYM_LN,SYM_LN,SYM_K,
                        SYM_N,SYM_FE,SYM_N,SYM_FE,SYM_J,
                        SYM_TR,SYM_K,SYM_Q,SYM_T,SYM_FE,
                        SYM_A,SYM_N,SYM_Q,SYM_K,SYM_A,
                        SYM_GF,SYM_J,SYM_GF,SYM_T,SYM_ZB,
                        SYM_K,SYM_Q,SYM_N,SYM_FE,SYM_J,
                        SYM_A,SYM_FE,SYM_GF,SYM_Q,SYM_LN,
                        SYM_WILD,SYM_ZB,SYM_K,SYM_LN,SYM_TR,
                        SYM_N,SYM_WILD,SYM_WILD,SYM_WILD,SYM_WILD,
                        SYM_Q,SYM_J,SYM_FE,SYM_LN,SYM_TR,
                        SYM_WILD,SYM_T,SYM_ZB,SYM_T,SYM_K,
                        SYM_T,SYM_ZB,SYM_N,SYM_T,SYM_A,
                        SYM_Q,SYM_GF,SYM_N,SYM_GF,SYM_A,
                        SYM_Q,SYM_Q,SYM_GF,
                },};
        static const uint8_t reelCarding_F1[MAX_NUM_REELS][MAX_NUM_STOPS] = {
                {
                        SYM_T,SYM_A,SYM_K,SYM_FE,SYM_SCAT,
                        SYM_J,SYM_N,SYM_TR,SYM_N,SYM_FE,
                        SYM_Q,SYM_GF,SYM_TR,SYM_ZB,SYM_N,
                        SYM_LN,SYM_SCAT,SYM_Q,SYM_N,SYM_Q,
                        SYM_J,SYM_GF,SYM_K,SYM_Q,SYM_A,
                        SYM_ZB,SYM_TR,SYM_A,SYM_N,SYM_ZB,
                        SYM_FE,SYM_LN,SYM_LN,SYM_LN,SYM_LN,
                        SYM_A,SYM_Q,SYM_LN,SYM_K,SYM_K,
                        SYM_ZB,SYM_ZB,SYM_TR,SYM_SCAT,SYM_TR,
                        SYM_Q,SYM_T,SYM_J,SYM_ZB,SYM_Q,
                        SYM_T,SYM_GF,SYM_GF,SYM_FE,SYM_J,
                        SYM_T,SYM_J,SYM_T,SYM_GF,SYM_LN,
                        SYM_LN,SYM_LN,SYM_LN,SYM_J,SYM_T,
                },
                {
                        SYM_N,SYM_ZB,SYM_K,SYM_TR,SYM_T,
                        SYM_LN,SYM_T,SYM_Q,SYM_LN,SYM_LN,
                        SYM_LN,SYM_LN,SYM_N,SYM_FE,SYM_ZB,
                        SYM_FE,SYM_ZB,SYM_J,SYM_TR,SYM_Q,
                        SYM_N,SYM_LN,SYM_WILD,SYM_WILD,SYM_WILD,
                        SYM_WILD,SYM_J,SYM_N,SYM_GF,SYM_GF,
                        SYM_N,SYM_A,SYM_FE,SYM_SCAT,SYM_T,
                        SYM_J,SYM_FE,SYM_A,SYM_K,SYM_T,
                        SYM_N,SYM_T,SYM_K,SYM_N,SYM_K,
                        SYM_A,SYM_LN,SYM_T,SYM_J,SYM_SCAT,
                        SYM_GF,SYM_FE,SYM_T,SYM_SCAT,SYM_TR,
                        SYM_Q,SYM_J,SYM_GF,SYM_LN,SYM_N,
                        SYM_ZB,SYM_T,SYM_TR,SYM_K,SYM_Q,
                },
                {
                        SYM_GF,SYM_FE,SYM_WILD,SYM_WILD,SYM_WILD,
                        SYM_WILD,SYM_A,SYM_K,SYM_J,SYM_N,
                        SYM_T,SYM_SCAT,SYM_TR,SYM_A,SYM_TR,
                        SYM_ZB,SYM_A,SYM_LN,SYM_LN,SYM_LN,
                        SYM_LN,SYM_K,SYM_FE,SYM_A,SYM_LN,
                        SYM_SCAT,SYM_T,SYM_A,SYM_TR,SYM_GF,
                        SYM_FE,SYM_N,SYM_TR,SYM_J,SYM_ZB,
                        SYM_A,SYM_Q,SYM_FE,SYM_SCAT,SYM_TR,
                        SYM_N,SYM_K,SYM_J,SYM_SCAT,SYM_J,
                        SYM_Q,SYM_FE,SYM_GF,SYM_K,SYM_ZB,
                        SYM_TR,SYM_LN,SYM_LN,SYM_LN,SYM_LN,
                        SYM_ZB,SYM_ZB,SYM_GF,SYM_GF,SYM_Q,
                        SYM_T,SYM_Q,SYM_T,SYM_ZB,SYM_GF,
                },
                {
                        SYM_FE,SYM_TR,SYM_Q,SYM_TR,SYM_GF,
                        SYM_N,SYM_J,SYM_WILD,SYM_FE,SYM_A,
                        SYM_J,SYM_TR,SYM_N,SYM_T,SYM_WILD,
                        SYM_WILD,SYM_WILD,SYM_WILD,SYM_J,SYM_GF,
                        SYM_ZB,SYM_TR,SYM_K,SYM_ZB,SYM_LN,
                        SYM_N,SYM_T,SYM_N,SYM_FE,SYM_T,
                        SYM_A,SYM_ZB,SYM_J,SYM_A,SYM_J,
                        SYM_TR,SYM_K,SYM_Q,SYM_LN,SYM_LN,
                        SYM_LN,SYM_LN,SYM_T,SYM_GF,SYM_T,
                        SYM_Q,SYM_ZB,SYM_T,SYM_Q,SYM_TR,
                        SYM_FE,SYM_Q,SYM_K,SYM_Q,SYM_T,
                        SYM_ZB,SYM_GF,SYM_ZB,SYM_GF,SYM_LN,
                        SYM_A,SYM_K,SYM_LN,SYM_LN,SYM_A,
                },
                {
                        SYM_T,SYM_ZB,SYM_N,SYM_J,SYM_ZB,
                        SYM_A,SYM_ZB,SYM_K,SYM_TR,SYM_T,
                        SYM_LN,SYM_LN,SYM_LN,SYM_LN,SYM_FE,
                        SYM_N,SYM_GF,SYM_FE,SYM_Q,SYM_T,
                        SYM_K,SYM_TR,SYM_T,SYM_A,SYM_TR,
                        SYM_GF,SYM_ZB,SYM_FE,SYM_N,SYM_FE,
                        SYM_T,SYM_ZB,SYM_Q,SYM_ZB,SYM_Q,
                        SYM_K,SYM_J,SYM_T,SYM_ZB,SYM_J,
                        SYM_FE,SYM_WILD,SYM_K,SYM_Q,SYM_LN,
                        SYM_K,SYM_GF,SYM_T,SYM_TR,SYM_A,
                        SYM_Q,SYM_LN,SYM_TR,SYM_LN,SYM_LN,
                        SYM_LN,SYM_LN,SYM_J,SYM_A,SYM_LN,
                        SYM_J,SYM_GF,SYM_ZB,SYM_J,SYM_LN,
                },
        };
#if 0
        // reelCarding_N1 debug stop positions
        {
                0,73,72,7,55,    //WILD
                        30,14,17,0,0,    //SCAT
                        20,5,0,44,20,    //LN
                        3,9,30,6,3,    //ZB
                        }
        // reelCarding_F1 debug stop positions
{
        0,22,2,7,41,   //WILD
                4,33,11,0,0,   //SCAT
                15,5,17,24,10,   //LN
                13,1,15,20,1,    //ZB
                }
#endif


}


#endif	/* _slot_game_personal_carding_h_ */

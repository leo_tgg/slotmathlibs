/******************************************************************************\
 * File    : RollingGoldData.h
 * Purpose :
\******************************************************************************/

#ifndef _slot_game_private_data_h_
#define _slot_game_private_data_h_

#include "SlotRule.hpp"

namespace rollingGold {
        using namespace slotrule;

        static constexpr uint8_t MAX_NUM_ROWS = 4;
        static constexpr uint8_t MAX_NUM_REELS = 5;
        static constexpr uint32_t SCREEN_SYMBOL_SIZE = MAX_NUM_REELS * MAX_NUM_ROWS;
        static constexpr uint32_t MAX_NUM_LINES = 50;
        static constexpr uint32_t MAX_NUM_STOPS = 88;
        static constexpr uint32_t SCATTER_TRIGGER_NUM = 3;
        static constexpr uint32_t FREESPIN_TRIGGERE_NUM = 10;
        static constexpr uint32_t FREESPIN_MULTIPLIER = 2;
        static constexpr uint32_t SCATTERPAY_TYPE = SCATTER_PAY_L2R;
        static constexpr uint32_t NUM_SUBSTITUTES = 1;
        static constexpr uint32_t NUM_SCATTERS = 1;

#include "RollingGoldSymbolDefine.hpp"


        static const SlotScatterPay  slotScatters[] = {
                {SYM_SCAT, {0, 0, 5, 0, 0}},
                {0xFF, {0, 0, 0, 0, 0}}		// end marker
        };


        static const SlotSubstitute slotSubstitutes[] = {

                // substitute 1
                {
                        // substitute
                        SYM_WILD,
                        {       // substitute for
                                {SYM_LN, 1},
                                {SYM_ZB, 1},
                                {SYM_GF, 1},
                                {SYM_TR, 1},
                                {SYM_FE, 1},
                                {SYM_A,  1},
                                {SYM_K,  1},
                                {SYM_Q,  1},
                                {SYM_J,  1},
                                {SYM_T,  1},
                                {SYM_N,  1},
                                {0xFF,   0}
                        }
                },
                // substitute 2
        };

        static const SlotPayTableEntry slotPayTable[] = {
                {  1000, {   1,   1,   1,   1,   1, }},
                {   250, {   1,   1,   1,   1, 255, }},
                {   100, {   1,   1,   1, 255, 255, }},
                {   500, {   2,   2,   2,   2,   2, }},
                {   200, {   2,   2,   2,   2, 255, }},
                {    80, {   2,   2,   2, 255, 255, }},
                {   500, {   3,   3,   3,   3,   3, }},
                {   200, {   3,   3,   3,   3, 255, }},
                {    80, {   3,   3,   3, 255, 255, }},
                {   400, {   4,   4,   4,   4,   4, }},
                {   150, {   4,   4,   4,   4, 255, }},
                {    60, {   4,   4,   4, 255, 255, }},
                {   400, {   5,   5,   5,   5,   5, }},
                {   150, {   5,   5,   5,   5, 255, }},
                {    60, {   5,   5,   5, 255, 255, }},
                {   400, {   6,   6,   6,   6,   6, }},
                {   100, {   6,   6,   6,   6, 255, }},
                {    40, {   6,   6,   6, 255, 255, }},
                {   200, {   7,   7,   7,   7,   7, }},
                {   100, {   7,   7,   7,   7, 255, }},
                {    40, {   7,   7,   7, 255, 255, }},
                {   200, {   8,   8,   8,   8,   8, }},
                {    50, {   8,   8,   8,   8, 255, }},
                {    20, {   8,   8,   8, 255, 255, }},
                {   150, {   9,   9,   9,   9,   9, }},
                {    50, {   9,   9,   9,   9, 255, }},
                {    20, {   9,   9,   9, 255, 255, }},
                {   150, {  10,  10,  10,  10,  10, }},
                {    20, {  10,  10,  10,  10, 255, }},
                {    10, {  10,  10,  10, 255, 255, }},
                {   100, {  11,  11,  11,  11,  11, }},
                {    20, {  11,  11,  11,  11, 255, }},
                {    10, {  11,  11,  11, 255, 255, }},
                {     0, {   0,   0,   0,   0,   0, }}
        };


}

#endif	/* _slot_game_private_data_h_ */

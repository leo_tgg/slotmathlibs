static const uint32_t slotGameScreenLines[MAX_NUM_ROWS] = {0, 12, 25, 38};

static const uint8_t slotLinesWhere[MAX_NUM_LINES][MAX_NUM_REELS]= {
        {ROW1, ROW1,  ROW1, ROW1, ROW1},
        {ROW1, ROW1,  ROW2, ROW1, ROW1},
        {ROW1, ROW2, ROW2, ROW2, ROW1},
        {ROW1, ROW2, ROW3, ROW2, ROW1},
        {ROW1, ROW2, ROW1, ROW2, ROW1},	// 5
        {ROW1, ROW1, ROW1, ROW2, ROW1},
        {ROW1, ROW2, ROW1, ROW1, ROW1},	//
        {ROW1, ROW1, ROW2, ROW2, ROW1},
        {ROW1, ROW2, ROW2, ROW1, ROW1},
        {ROW1, ROW1, ROW3, ROW1, ROW1},     //10
        {ROW1, ROW1, ROW3, ROW2, ROW1},
        {ROW1, ROW2, ROW3, ROW1, ROW1},
        {ROW2, ROW2, ROW2, ROW2, ROW2},
        {ROW2, ROW2, ROW3, ROW2, ROW2},
        {ROW2, ROW2, ROW1, ROW2, ROW2},       //15
        {ROW2, ROW3, ROW3, ROW3, ROW2},
        {ROW2, ROW1, ROW1, ROW1, ROW2},
        {ROW2, ROW3, ROW2, ROW3, ROW2},
        {ROW2, ROW1, ROW2, ROW1, ROW2},
        {ROW2, ROW2, ROW2, ROW3, ROW2},    //20
        {ROW2, ROW2, ROW2, ROW1, ROW2},
        {ROW2, ROW3, ROW2, ROW2, ROW2},
        {ROW2, ROW1, ROW2, ROW2, ROW2},
        {ROW2, ROW3, ROW1, ROW3, ROW2},
        {ROW2, ROW1, ROW3, ROW1, ROW2},     //25
        {ROW3, ROW3, ROW3, ROW3, ROW3},
        {ROW3, ROW3, ROW4, ROW3, ROW3},
        {ROW3, ROW3, ROW2, ROW3, ROW3},
        {ROW3, ROW4, ROW4, ROW4, ROW3},
        {ROW3, ROW2, ROW2, ROW2, ROW3},   //30
        {ROW3, ROW4, ROW3, ROW4, ROW3},
        {ROW3, ROW2, ROW3, ROW2, ROW3},
        {ROW3, ROW3, ROW3, ROW4, ROW3},
        {ROW3, ROW3, ROW3, ROW2, ROW3},
        {ROW3, ROW4, ROW3, ROW3, ROW3},      //35
        {ROW3, ROW2, ROW3, ROW3, ROW3},
        {ROW3, ROW4, ROW2, ROW4, ROW3},
        {ROW3, ROW2, ROW4, ROW2, ROW3},
        {ROW4, ROW4, ROW4, ROW4, ROW4},
        {ROW4, ROW4, ROW3, ROW4, ROW4},      //40
        {ROW4, ROW3, ROW3, ROW3, ROW4},
        {ROW4, ROW3, ROW2, ROW3, ROW4},
        {ROW4, ROW3, ROW4, ROW3, ROW4},
        {ROW4, ROW4, ROW4, ROW3, ROW4},
        {ROW4, ROW3, ROW4, ROW4, ROW4},     //45
        {ROW4, ROW4, ROW3, ROW3, ROW4},
        {ROW4, ROW3, ROW3, ROW4, ROW4},
        {ROW4, ROW4, ROW2, ROW4, ROW4},
        {ROW4, ROW4, ROW2, ROW3, ROW4},
        {ROW4, ROW3, ROW2, ROW4, ROW4}      //50
};

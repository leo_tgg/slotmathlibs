#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <string>
#include <cstring>
#include <cstdint>
#include "SlotUtility.hpp"
#include "SlotRule.hpp"
#include "RollingGoldRule.hpp"
#include "RollingGoldCarding.hpp"
#include "RollingGoldData.hpp"

namespace rollingGold {
        using namespace slotrule;
#include "RollingGoldLines.hpp"
        static inline uint32_t getReelStop(uint8_t var, uint8_t reelPaySet, uint8_t reel)
        {
                switch (var) {
                case 0:
                        switch(reelPaySet) {
                        case NORMAL_SPIN:	return reelStops_N1[reel];
                        case FREE_SPIN:	return reelStops_F1[reel];
                        default:
                                std::cerr << "getReelStop reelPaySet error\n";
                                break;
                        }
                        break;
                case 1:
                default:
                        std::cerr << "getReelStop var error\n";
                        break;

                }

                return 0;
        }

        static inline const uint8_t *getReel(uint8_t var, uint8_t reelPaySet, uint8_t reel)
        {
                switch (var) {
                case 0:
                        switch(reelPaySet) {
                        case NORMAL_SPIN:	return reelCarding_N1[reel];
                        case FREE_SPIN:	return reelCarding_F1[reel];
                        default:
                                std::cerr << "getReel reelPaySet error\n";
                                break;
                        }
                        break;
                case 1:
                default:
                        std::cerr << "getReel var error\n";
                        break;
                }

                return NULL;
        }

        /************************************************************\
         *    End of Helper functions
         ************************************************************/


        static const char **getSymbolName(void)
        {
                static const char *ns[] = {
                        "LN",
                        "ZB",
                        "GF",
                        "TR",
                        "FE",
                        "A",
                        "K",
                        "Q",
                        "J",
                        "T",
                        "N",
                        "WILD",
                        "SCAT",
                        NULL
                };

                return ns;
        }
        static const uint8_t *getSymbolCode(void)
        {
                static const uint8_t codeSym[] = {
                        SYM_LN,
                        SYM_ZB,
                        SYM_GF,
                        SYM_TR,
                        SYM_FE,
                        SYM_A,
                        SYM_K,
                        SYM_Q,
                        SYM_J,
                        SYM_T,
                        SYM_N,
                        SYM_WILD,
                        SYM_SCAT,
                        0,
                };

                return codeSym;
        }


        static uint8_t getSymbolNum(void)
        {
                return NUM_SYMBOLS;
        }


        static const char *getGameName(void)
        {
                return "ROLLING GOLD";
        }

        static const char *rtps[] = {
                "90.126",
                NULL
        };
        static const char **getVariations(void)
        {
                return rtps;
        }

        static uint8_t getVarNum()
        {
                static uint8_t vs = 0;

                if (!vs) {
                        const char **rtp = rtps;
                        while (*rtp) {
                                ++vs;
                                ++rtp;
                        }
                        return vs;
                } else {
                        return vs;
                }
        }


        static const char **getNameGames(void)
        {
                static const char *ngs[] = {
                        "Normal Game",
                        "Free Game",
                        NULL
                };
                return ngs;
        }


        static inline uint32_t getMaxLines(void)
        {
                return MAX_NUM_LINES;
        }

        static inline uint8_t getMaxReels(void)
        {
                return MAX_NUM_REELS;
        }


        static inline uint8_t getMaxRows(void)
        {
                return MAX_NUM_ROWS;
        }

        static inline uint32_t getScreenSymbolSize(void)
        {
                return SCREEN_SYMBOL_SIZE;
        }


        /*
         */
        static void buildLineSymbol(uint8_t var, uint8_t reelPaySet, const uint8_t *stops, uint32_t lnum, uint8_t *line)
        {
                uint32_t j, reelStop;
                for (uint8_t i=0; i<MAX_NUM_REELS; ++i) {
                        reelStop = getReelStop(var, reelPaySet, i);
                        j = stops[i] + slotLinesWhere[lnum][i];
                        j %= reelStop;
                        line[i] = getReel(var, reelPaySet, i)[j];
                }

        }

        /*
         */
        static void buildScreenSymbol(uint8_t var, uint8_t reelPaySet, const uint8_t *stops, uint8_t *screenSym)
        {
                //std::cerr << "buildScreenSymbol begin\n";
                // get symbole in the line
                for (uint8_t i=0; i < MAX_NUM_ROWS; ++i) {
                        buildLineSymbol(var, reelPaySet, stops, slotGameScreenLines[i], screenSym+i*MAX_NUM_REELS);
                }
        }

        static void buildStops(uint8_t var, uint8_t reelPaySet, uint8_t *stops)
        {
                for (uint8_t i=0; i<MAX_NUM_REELS; ++i) {
                        uint32_t reelStop = getReelStop(var, reelPaySet, i);
                        stops[i] = slot::random(0, reelStop-1);
                }
        }

        /*
         */
        static inline void buildLinePosition(uint32_t lnum, uint8_t *linePos)
        {
                memcpy(linePos, slotLinesWhere[lnum], MAX_NUM_REELS);
        }
        /*
         * Detecting
         */
        static uint32_t detectFeatureAll(uint8_t reelPaySet, uint8_t *screenSym)
        {
                // only retrigger once
                if ( (reelPaySet == NORMAL_SPIN) || (reelPaySet == FREE_SPIN)) {
                        //|| (status->state == GT_FREESPIN && !status->retriggered)) {
                        uint32_t count = 0;
                        for (uint32_t i=0; i< SCREEN_SYMBOL_SIZE; ++i) {
                                if (screenSym[i] == SCATTER_SYMBOBL)
                                        ++count;
                        }
                        if (count >= SCATTER_TRIGGER_NUM) {
                                return 1;
                        }

                }

                return 0;
        }
        /*
         * Detecting
         */
        static uint32_t detectFeatureLine(uint8_t reelPaySet, uint8_t *line)
        {
                (void)reelPaySet;
                (void)line;
                return 0;
        }
        /*
         */
        static const SlotPayTableEntry * getPayTable(uint8_t reelPaySet)
        {
                (void)reelPaySet;
                return slotPayTable;
        }
        /*
         */
        static uint32_t getScatterPayType(void)
        {
                return SCATTERPAY_TYPE;
        }

        /*
         *
         */
        static uint32_t getScatterNum(void)
        {
                return NUM_SCATTERS;
        }
        /*
         */
        static const SlotScatterPay *getScatter(uint32_t index)
        {
                if (index >= NUM_SCATTERS)
                        return NULL;

                return &slotScatters[index];
        }

        /*
         */
        static const SlotSubstitute *getSubstitue(uint8_t symbol)
        {
                for (uint32_t i=0; i< NUM_SUBSTITUTES; ++i)
                        if (slotSubstitutes[i].sub == symbol)
                                return &slotSubstitutes[i];

                return NULL;
        }

        static const SlotFeatureParam& getFeatureParam(uint8_t reelPaySet)
        {
                (void)reelPaySet;
                static const SlotFeatureParam fp = {
                        FREESPIN_MULTIPLIER
                };

                return fp;
        }


        /*****
         * getASubsBMul
         *
         * Check is symbol A can substitue for symbol B.  If
         * so, get the multiplier value.  If A==B, then
         * returns 1
         *
         * Input:
         *  a           symbol A
         *  b           symbol B
         *  reelPaySet  reel strip & pay table set
         *
         * Returns:
         *  multiplier value if A subs for B, else returns 0
         */
        static uint32_t getASubsBMul(uint8_t a, uint8_t b)
        {
                const SlotSubstitute *sub;

                if(a==b) return 1;

                if((sub=getSubstitue(a))==NULL)
                        return 0;
                for(uint32_t i=0;i<NUM_SYMBOLS;++i) {
                        if(sub->replaces[i].symbol==b)
                                return sub->replaces[i].multiplier;
                        else if(sub->replaces[i].symbol== 0xFF)
                                return 0;
                }

                return 0;
        }



        /*****
         * getASubsBSym
         */
        static uint32_t getASubsBSym(uint8_t a, uint8_t b)
        {
                const SlotSubstitute *sub;

                if(a==b) return a;

                if((sub=getSubstitue(a))==NULL)
                        return 0xFF;
                for(uint32_t i=0;i<NUM_SYMBOLS; ++i) {
                        if(sub->replaces[i].symbol==b)
                                return sub->replaces[i].symbol;
                        else if(sub->replaces[i].symbol==0xFF)
                                return 0xFF;
                }

                return 0;
        }


        static uint32_t nonLinePay(uint8_t reelPaySet, uint32_t numSelectedLines, const uint8_t *screenSym, NonLineWin *win)
        {
                uint32_t x,y;
                uint32_t scatNum, scat, count;
                uint32_t pay, totPay;
                uint32_t scatterOnReel;
                uint32_t mult, tempMult;

                totPay=0;
                switch(getScatterPayType()) {
                case SCATTER_PAY_L2R:
                        for(scatNum=0; scatNum<getScatterNum(); ++scatNum) {
                                // Count the number of scattered symbols
                                scat=getScatter(scatNum)->name;
                                if(scat==0xFF) //end of scatters?
                                        break; //get out of scatter calc

                                count=0;
                                mult=0;
                                for(scatterOnReel=1,x=0; ((scatterOnReel)&&(x<MAX_NUM_REELS)); ++x) {
                                        scatterOnReel=0;
                                        for(y=0; y<MAX_NUM_ROWS; ++y) {
                                                if((tempMult=getASubsBMul(screenSym[y*MAX_NUM_REELS+x],scat)) != 0) {
                                                        ++count;
                                                        scatterOnReel=1;
                                                        if(mult<tempMult) mult=tempMult;
                                                }
                                        }
                                }

                                // Is the number enough to pay
                                if(count==0)  continue;
                                if((pay=getScatter(scatNum)->pays[count-1]*mult) != 0)
                                        totPay+=pay;

                                if(!pay) continue;

                                win[scatNum].winCr = pay * numSelectedLines;
                                if (pay && reelPaySet == GT_FREESPIN) {
                                        win[scatNum].winCr *= getFeatureParam(reelPaySet).multiplier;
                                        pay *= getFeatureParam(reelPaySet).multiplier;
                                        win[scatNum].multiplier = getFeatureParam(reelPaySet).multiplier;
                                } else {
                                        win[scatNum].multiplier = 1;
                                }

                                // Set the animation flags
                                for(scatterOnReel=1,x=0; ((scatterOnReel)&&(x<MAX_NUM_REELS)); ++x) {
                                        scatterOnReel=0;
                                        for(y=0; y<MAX_NUM_ROWS; y++) {
                                                uint8_t sym = screenSym[y*MAX_NUM_REELS+x];
                                                if(getASubsBMul(sym,scat)) {
                                                        scatterOnReel=1;
                                                        win[scatNum].symbols[y*MAX_NUM_REELS+x] = getASubsBSym(sym, scat);
                                                } else {
                                                        win[scatNum].symbols[y*MAX_NUM_REELS+x] = 0xFF;
                                                }
                                        }
                                        //lmuSetMaskBit(wins->nonLineWinMask, scatNum);
                                }
                        } // for
                        break;

                case SCATTER_PAY_ANYPAY:
                        for(scatNum=0; scatNum<getScatterNum(); ++scatNum) {
                                // Count the number of scattered symbols
                                scat=getScatter(scatNum)->name;
                                if(scat==0xFF) //end of scatters?
                                        break; //get out of scatter calc

                                count=0;
                                mult=0;
                                for (x=0; x < MAX_NUM_REELS; x++)
                                        for (y=0; y < MAX_NUM_ROWS; y++) {
                                                if ( (tempMult = getASubsBMul(screenSym[y*MAX_NUM_REELS+x], scat)) != 0 ) {
                                                        ++count;
                                                        if (mult < tempMult) mult = tempMult;
                                                }
                                        }

                                // Is the number enough to pay
                                if(count==0) continue;
                                if((pay=getScatter(scatNum)->pays[count-1]*mult) != 0)
                                        totPay+=pay;

                                if(!pay) continue;


                                win[scatNum].winCr = pay * numSelectedLines;
                                if (pay && reelPaySet == GT_FREESPIN) {
                                        win[scatNum].winCr *= getFeatureParam(reelPaySet).multiplier;
                                        pay *= getFeatureParam(reelPaySet).multiplier;
                                        win[scatNum].multiplier = getFeatureParam(reelPaySet).multiplier;
                                } else {
                                        win[scatNum].multiplier = 1;
                                }

                                // Set the animation flags
                                for (x=0; x < MAX_NUM_REELS; x++)
                                        for (y=0; y < MAX_NUM_ROWS; y++) {
                                                uint8_t sym = screenSym[y*MAX_NUM_REELS+x];
                                                if(getASubsBMul(sym, scat)) {
                                                        win[scatNum].symbols[y*MAX_NUM_REELS+x] = getASubsBSym(sym, scat);
                                                } else {
                                                        win[scatNum].symbols[y*MAX_NUM_REELS+x] = 0xFF;
                                                }
                                                //lmuSetMaskBit(wins->nonLineWinMask, scatNum);
                                        }

                        } // for
                        break;

                default:
                        return 0;
                }

                return totPay * numSelectedLines;
        }


        static uint32_t linePay(const SlotGameData& gameData, uint32_t lnum, LineWin& win, TriggerData& feature)
        {
                uint8_t winSymbols[MAX_NUM_REELS], tempWinSymbols[MAX_NUM_REELS];
                const SlotPayTableEntry *payTable;
                uint32_t payI;
                uint32_t lineWin;
                uint32_t reel;
                uint32_t multiplier, tempMultiplier;
                uint32_t triggered;
                uint8_t  line[MAX_NUM_REELS];
                uint8_t  symB;


                //printf("line pay begins\n");
                buildLineSymbol(gameData.var, gameData.reelPaySet, gameData.stops, lnum, line);
                //printf("end of buildLineSymbol\n");

                lineWin=0;

                payTable = getPayTable(gameData.reelPaySet);
                //printf("end of payTable\n");

                if( payTable != NULL ) {
                        // Calculate number of credits won on line
                        // Loop through every pay table entry looking
                        // for the greatest match
                        for(payI=0; payTable[payI].paysCr!=0; ++payI) {
                                multiplier=1;
                                memset(tempWinSymbols, 0xFF, sizeof(tempWinSymbols));
                                // Check for a match
                                for(reel=0; reel<MAX_NUM_REELS; ++reel) {
                                        if((symB=payTable[payI].symbol[reel])!=DONT_CARE) {
                                                //printf("line pay here for reel \n");
                                                if((tempMultiplier=getASubsBMul(line[reel],symB))!=0) {
                                                        multiplier=slot::max(multiplier,tempMultiplier);
                                                        tempWinSymbols[reel]=getASubsBSym(line[reel],symB);
                                                } else break;
                                        }
                                }
                                //printf("line pay here\n");
                                // Matched ?
                                if(reel==MAX_NUM_REELS) {
                                        if(lineWin < payTable[payI].paysCr*multiplier) {
                                                lineWin=payTable[payI].paysCr*multiplier;
                                                memcpy(winSymbols, tempWinSymbols, sizeof(winSymbols));
                                        }
                                }
                        } /* end of  for(payI=0; payTable[payI].paysCr!=0; payI++) */

                        // Check if line triggered a triggered
                        triggered=detectFeatureLine(gameData.reelPaySet, line);
                        //printf("end of detectFeatureLine\n");
                        // Setup winning symbols
                        if(triggered) {
                                for(reel=0; reel<MAX_NUM_REELS; ++reel) {
                                        if(line[reel]&0x80) {
                                                winSymbols[reel] = line[reel]&0x7F;
                                        }
                                }
                                feature.type = GT_FREESPIN;
                                feature.chances = FREESPIN_TRIGGERE_NUM;
                                feature.multiplier = FREESPIN_MULTIPLIER;
                        }
                }

                if (lineWin) {
                        if (gameData.reelPaySet == GT_FREESPIN)
                                lineWin *= getFeatureParam(gameData.reelPaySet).multiplier;

                        // record win data
                        win.winCr = lineWin;
                        buildLinePosition(lnum, win.position);
                        win.lineIndex = lnum;
                        uint32_t winSymNum = 0;
                        for (uint8_t i = 0; i < MAX_NUM_REELS; ++i) {
                                if (winSymbols[i] != 0xFF)
                                        ++winSymNum;
                        }
                        win.winSymNum =winSymNum;
                }

                return lineWin;
        }

        static uint32_t doCalcWin(SlotGameData& gameData, SlotWinData& win)
        {
                uint32_t feature = detectFeatureAll(gameData.reelPaySet, gameData.symbols);
                if (feature) {
                        win.feature.type = GT_FREESPIN;
                        win.feature.chances = FREESPIN_TRIGGERE_NUM;
                        win.feature.multiplier = FREESPIN_MULTIPLIER;
                }

                uint32_t lineWinCr = 0;
                for (uint32_t lnum=0; lnum < gameData.numSelectedLines; ++lnum) {
                        LineWin lineWin;
                        lineWinCr += linePay(gameData, lnum, lineWin, win.feature);
                        if (lineWin.winCr) {
                                win.lineWin.push_back(lineWin);
                        }

                }

                NonLineWin sctWin[NUM_SCATTERS+1];
                uint32_t nonLineWinCr = nonLinePay(gameData.reelPaySet, gameData.numSelectedLines,
                                                   gameData.symbols,sctWin);
                if (nonLineWinCr) {
                        win.nonLineWin.push_back(std::move(sctWin[0]));
                }
                win.winCr = lineWinCr + nonLineWinCr;

                return (lineWinCr + nonLineWinCr);
        }

        void buildInitScreenSymbol(uint8_t var, vector<uint8_t>& screenSym)
        {
                uint8_t stops[MAX_NUM_REELS];
                memset(stops, 0, sizeof(stops));
                uint8_t sym[SCREEN_SYMBOL_SIZE];
                buildScreenSymbol(var, 0, stops, sym);
                std::copy(sym, sym+SCREEN_SYMBOL_SIZE, std::back_inserter(screenSym));

        }


        uint32_t calcWin(SlotGameData& gameData, SlotWinData& win)
        {
                win.reels = MAX_NUM_REELS;
                win.rows = MAX_NUM_ROWS;

                switch (gameData.reelPaySet) {
                case FREE_SPIN:
                        win.multiplier = FREESPIN_MULTIPLIER;
                case NORMAL_SPIN:
                        buildStops(gameData.var, gameData.reelPaySet, gameData.stops);
                        buildScreenSymbol(gameData.var, gameData.reelPaySet, gameData.stops, gameData.symbols);
                        doCalcWin(gameData, win);
                case GT_GAMBLE:
                        break;
                default:
                        break;
                }

                return 0;
        }

}

namespace slotrule {
        void RollingGoldCalc::buildInitScreenSymbol(uint8_t var, vector<uint8_t>& screenSym)
        {
                rollingGold::buildInitScreenSymbol(var, screenSym);
        }

        uint32_t RollingGoldCalc::calculateWin(SlotGameData& gameData, SlotWinData& win)
        {
                return rollingGold::calcWin(gameData, win);
        }
}

/******************************************************************************\
 * File    : RollingGoldSymbolDefine.hpp
 * Purpose :
\******************************************************************************/

#pragma once
namespace rollingGold {

        enum RollingGoldSymbol {
                SYM_LN	=	1	,
                SYM_ZB	=	2,
                SYM_GF	=	3,
                SYM_TR	=	4,
                SYM_FE =	5,
                SYM_A	=	6,
                SYM_K	=	7,
                SYM_Q	=	8,
                SYM_J	=	9,
                SYM_T	=	10,
                SYM_N	=	11,
                SYM_WILD =	12,
                SYM_SCAT = 13,
        };
        static constexpr uint32_t NUM_SYMBOLS	= 13;
        static constexpr uint8_t SCATTER_SYMBOBL	= SYM_SCAT;
}


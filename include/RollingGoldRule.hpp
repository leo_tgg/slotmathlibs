#pragma once
#include <vector>
#include <string>
#include <cstring>
#include <cstdint>

namespace slotrule {
        class RollingGoldCalc : public SlotGameCalc {
        public:
                static RollingGoldCalc* instance()
                {
                        static RollingGoldCalc * rg= new RollingGoldCalc;
                        return rg;
                }

                void buildInitScreenSymbol(uint8_t var, vector<uint8_t>& screenSym);
                uint32_t calculateWin(SlotGameData& gameData, SlotWinData& win);

        private:
                RollingGoldCalc() {}
        };
}

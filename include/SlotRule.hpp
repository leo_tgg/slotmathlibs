#pragma once
#include <cstdint>
#include <vector>

namespace slotrule{
        using std::vector;
        static constexpr uint32_t slot_max_reel_size = 5;
        static constexpr uint32_t slot_max_row_size = 5;
        static constexpr uint32_t slot_max_num_symbol = 20;
        static constexpr uint8_t DONT_CARE = 0xFF; // don't care symbol
        enum GameType {
                GT_NONE,
                GT_NORMALSPIN,
                GT_FREESPIN,
                GT_FREESPIN2,
                GT_FEATURE_BONUS,
                GT_FEATURE_BONUS2,
                GT_GAMBLE,
                GT_HELP,
                GT_GAMESELECTION,
        };

        enum ReelPaySetType {
                NORMAL_SPIN,
                FREE_SPIN,
                FREE_SPIN2,
                BONUS1,
                BONUS2,
                GAMBLE
        };

        struct TriggerData {
                uint32_t type{0};
                uint32_t chances;
                uint32_t multiplier;
        };

        struct LineWin {
                uint32_t winCr{0};
                uint8_t position[slot_max_reel_size];
                uint32_t lineIndex;
                uint32_t winSymNum;
        };

        struct NonLineWin {
                uint32_t winCr{0};
                uint8_t symbols[slot_max_reel_size  * slot_max_reel_size];
                uint32_t multiplier;
        };

        struct GambleData {
                uint32_t aiBetOn;
        };

        struct SlotWinData {
                uint32_t id{0};
                uint32_t reels{0};
                uint32_t rows{0};
                uint32_t winCr{0};
                uint32_t multiplier{1};
                TriggerData feature;
                vector<LineWin> lineWin;
                vector<NonLineWin> nonLineWin;
                GambleData gamble;
        };

        enum SlotScatterPayType {
                SCATTER_PAY_L2R, // pay is left to right
                SCATTER_PAY_ANYPAY, // pay is anywhere on the line
                SCATTER_PAY_EACHWAY, // pay is left 2 right and right to left
        };

        struct SlotSubstitutedSymbol {
                uint8_t symbol; // symbol substituted for
                uint32_t multiplier; // win multiplier
        };

        struct SlotSubstitute {
                uint8_t sub;
                SlotSubstitutedSymbol replaces[slot_max_num_symbol];
        };

        struct SlotPayTableEntry {
                uint32_t paysCr;
                uint8_t symbol[slot_max_reel_size];
        };

        struct SlotScatterPay{
                uint8_t name;
                uint32_t pays[slot_max_reel_size * slot_max_row_size];
        };

        enum SlotLinePattern {
                ROW1,
                ROW2,
                ROW3,
                ROW4,
                ROW5,
        };

        struct SlotFeatureParam {
                const uint32_t multiplier;
        };

        struct SlotGameData{
                uint8_t var;
                uint8_t reelPaySet;
                uint32_t numSelectedLines;
                uint8_t stops[slot_max_row_size * slot_max_reel_size];
                uint8_t symbols[slot_max_row_size * slot_max_reel_size];
        };

        class SlotGameCalc {
        public:
                ~SlotGameCalc() {}

                virtual void buildInitScreenSymbol(uint8_t var, vector<uint8_t>& screenSym) = 0;
                virtual uint32_t calculateWin(SlotGameData& gameData, SlotWinData& win) = 0;
        };
}

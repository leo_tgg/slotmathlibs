#pragma once
#include <string>
#include <cstdint>

namespace slotrule {
        void slotRuleCalc(uint32_t gameID, uint32_t var, std::string& data);
        void slotRuleCalc(uint32_t gameID, uint32_t var, uint32_t gameType, uint32_t numLineSelected, std::string& data);
};




/****
 *
 */
#pragma once
#include <cstdint>

namespace slot {

        uint32_t random(uint32_t min, uint32_t max);
        void mt_setrand(uint32_t seed);
        uint32_t mt_getrand(void);
        static inline uint32_t max(uint32_t a, uint32_t b) {return (a > b ? a : b);}

};

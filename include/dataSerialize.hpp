/*****************************************************************************\
 * File: dataSerialize.hpp
 * Purpose:
 \****************************************************************************/
#pragma once
#include <string>
#include <vector>
#include <cstdint>
#include "SlotRule.hpp"

namespace datasl {

        void serialize(const slotrule::SlotGameData &gameData, const slotrule::SlotWinData& win, std::string& buf);
        void serialize(const std::vector<uint8_t>& screenSym, std::string& buf);
}

#include <iostream>
#include <vector>
#include <string>
#include "SlotRule.hpp"
#include "SlotRuleLib.hpp"
#include "RollingGoldRule.hpp"
#include "dataSerialize.hpp"

namespace slotrule{
        struct SlotRuleLibs {
                uint32_t id;
                SlotGameCalc* g;
        };

        SlotRuleLibs srLibs[] = {
                {1, RollingGoldCalc::instance()},
        };


#ifndef array_size
#define array_size(a) (sizeof(a)/sizeof(a[0]))
#endif

        void slotRuleCalc(uint32_t gameID, uint32_t var, std::string& data)
        {
                for (uint32_t i = 0; i< array_size(srLibs); ++i) {
                        if (srLibs[i].id == gameID) {
                                vector<uint8_t> screenSym;
                                srLibs[i].g->buildInitScreenSymbol(var, screenSym);
                                datasl::serialize(screenSym, data);
                                return;
                        }
                }
                data = "";

        }

        void slotRuleCalc(uint32_t gameID, uint32_t var, uint32_t gameType, uint32_t numSelectedLines, std::string& data)
        {
                SlotWinData win;
                SlotGameData gameData;

                gameData.var = var;
                gameData.reelPaySet = gameType-1; // reelPaySet start from 0
                gameData.numSelectedLines = numSelectedLines;
                win.id = gameID;

                for (uint32_t i = 0; i< array_size(srLibs); ++i) {

                        if (srLibs[i].id == gameID) {
                                srLibs[i].g->calculateWin(gameData, win);
                                datasl::serialize(gameData, win, data);
                                return;
                        }
                }

                data = "";
        }
}

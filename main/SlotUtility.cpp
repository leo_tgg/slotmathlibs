/******
 *
 */

#include <ctime>
#include "SlotUtility.hpp"

namespace slot {

        /* Period parameters */
        static constexpr uint16_t N = 624;
        static constexpr uint16_t M = 397;
        static constexpr uint32_t  MATRIX_A = 0x9908B0DF;   /* constexpr vector a */
        static constexpr uint32_t UPPER_MASK =  0x80000000;  /* most significant w-r bits */
        static constexpr uint32_t LOWER_MASK = 0x7FFFFFFF;   /* least significant r bits */

        /* Tempering parameters */
        static constexpr uint32_t TEMPERING_MASK_B = 0x9D2C5680;
        static constexpr uint32_t TEMPERING_MASK_C = 0xEFC60000;
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

        static uint32_t _mt[N];
        static uint16_t _mti = N+1;
        static uint64_t _trand;

        void mt_setrand(uint32_t seed)
        {
                /* setting initial seeds to _mt[N] using         */
                /* the generator Line 25 of Table 1 in          */
                /* [KNUTH 1981, The Art of Computer Programming */
                /*    Vol. 2 (2nd Ed.), pp102]                  */
                _mt[0]= seed & 0xFFFFFFFF;
                for (_mti=1; _mti<N; _mti++)
                        _mt[_mti] = (69069 * _mt[_mti-1]) & 0xFFFFFFFF;
        }
        uint32_t mt_getrand(void)
        {
                uint32_t y,seed;
                static uint32_t mag01[2]={0x0, MATRIX_A};

                if (_mti >= N) { /* generate N words at one time */
                        int kk;

                        if (_mti == N+1) {   /* if MT_setRand() has not been called, */
                                do {
                                        // default seed after power on
                                        seed=(uint32_t)(time(NULL)^clock())+ (uint32_t)_trand;
                                } while(seed==0);
                                mt_setrand(seed);
                                _trand = (uint64_t)seed;
                        }
                        for (kk=0;kk<N-M;kk++) {
                                y = (_mt[kk]&UPPER_MASK)|(_mt[kk+1]&LOWER_MASK);
                                _mt[kk] = _mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
                        }
                        for (;kk<N-1;kk++) {
                                y = (_mt[kk]&UPPER_MASK)|(_mt[kk+1]&LOWER_MASK);
                                _mt[kk] = _mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
                        }
                        y = (_mt[N-1]&UPPER_MASK)|(_mt[0]&LOWER_MASK);
                        _mt[N-1] = _mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];

                        _mti = 0;
                }

                y = _mt[_mti++];
                y ^= TEMPERING_SHIFT_U(y);
                y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
                y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
                y ^= TEMPERING_SHIFT_L(y);

                return y;
        }


        uint32_t random(uint32_t min, uint32_t max)
        {
                uint32_t trand, max32;

                max32=0xFFFFFFFF/(max-min+1)*(max-min+1);
                do {
                        trand=mt_getrand();
                }while(trand>max32);

                if ((uint64_t)trand != _trand) {
                        _trand = trand;
                }

                // scale the random number
                return trand%(max-min+1)+min;
        }

}

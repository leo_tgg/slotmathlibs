#include<cstring>
#include<string>
#include "tgg_game_math_TGGGameMathWebUtils.h"
#include "SlotRuleLib.hpp"


/*
 * Class:     tgg_game_math_TGGGameMathWebUtils
 * Method:    InitGameHere
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_tgg_game_math_TGGGameMathWebUtils_InitGameHere
(JNIEnv *env, jclass jc, jint id, jint rtp)
{
        (void)jc;
        std::string data;
        slotrule::slotRuleCalc((uint32_t)id, (uint32_t)rtp, data);
        const char* pat = data.c_str();
        // char* 转 string
        //定义java String类 strClass
        jclass strClass = (env)->FindClass("Ljava/lang/String;");
        //获取String(byte[],String)的构造器,用于将本地byte[]数组转换为一个新String
        jmethodID ctorID = (env)->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
        //建立byte数组
        jbyteArray bytes = (env)->NewByteArray(strlen(pat));
        //将char* 转换为byte数组
        //(env)->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*) pat);
        (env)->SetByteArrayRegion(bytes, 0, strlen(pat), const_cast<jbyte *>(reinterpret_cast<const jbyte*>(pat)));
        // 设置String, 保存语言类型,用于byte数组转换至String时的参数
        jstring encoding = (env)->NewStringUTF("UTF-8");
        //将byte数组转换为java String,并输出
        return (jstring) (env)->NewObject(strClass, ctorID, bytes, encoding);
}


/*
 * Class:     tgg_game_math_TGGGameMathWebUtils
 * Method:    CalcWinHere
 * Signature: (IIII)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_tgg_game_math_TGGGameMathWebUtils_CalcWinHere
(JNIEnv *env, jclass jc, jint id, jint type, jint rtp, jint numLineSelected)
{
        (void)jc;
        std::string data;
        slotrule::slotRuleCalc((uint32_t)id, (uint32_t)rtp, (uint32_t)type, (uint32_t)numLineSelected, data);
        const char *pat = data.c_str();
        // char* 转 string
        //定义java String类 strClass
        jclass strClass = (env)->FindClass("Ljava/lang/String;");
        //获取String(byte[],String)的构造器,用于将本地byte[]数组转换为一个新String
        jmethodID ctorID = (env)->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
        //建立byte数组
        jbyteArray bytes = (env)->NewByteArray(strlen(pat));
        //将char* 转换为byte数组
        (env)->SetByteArrayRegion(bytes, 0, strlen(pat), const_cast<jbyte *>(reinterpret_cast<const jbyte*>(pat)));
        // 设置String, 保存语言类型,用于byte数组转换至String时的参数
        jstring encoding = (env)->NewStringUTF("UTF-8");
        //将byte数组转换为java String,并输出
        return (jstring) (env)->NewObject(strClass, ctorID, bytes, encoding);
}

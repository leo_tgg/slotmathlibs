/*****************************************************************************\
 * File: Serialization.cpp
 * Purpose:
 \****************************************************************************/

#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include "prettywriter.h"
#include "dataSerialize.hpp"

namespace datasl {
        using namespace rapidjson;
        using namespace slotrule;

        /*
          class Data2Json {
          public:
          Data2Json() {}
          virtual ~Data2Json() {}

          // not work
          template<typename Writer>
          void serializeHelp(Writer &write, uint8_t totalPages, uint8_t idx) const;
          template<typename Writer>
          void serializeGamble(Writer &write, const gls::GlsData *data) const;
          template<typename Writer>
          void serializeMeter(Writer &write, const gls::GlsData *data) const;
          template<typename Writer>
          void serializeFlag(Writer &write, const gls::GlsData *data) const;
          template<typename Writer>
          void serializeFreespin(Writer &write, const gls::GlsData *data) const;

          template<typename Writer>
          void serializeLineWin(Writer &write, const gls::GlsData *data) const;

          template<typename Writer>
          void serializeMessage(Writer &write, const char *msg) const;

          template<typename Writer>
          void serializeMessage(Writer &write, const std::string *msg) const;


          };

          template<typename Writer>
          void Data2Json::serializeHelp(Writer &writer, uint8_t totalPages, uint8_t idx) const
          {
          writer.StartObject();
          writer.Key("pages");
          writer.Ubyte(totalPages);
          writer.Key("index");
          writer.Ubyte(idx);
          writer.EndObject();
          }

          template<typename Writer>
          void Data2Json::serializeGamble(Writer &writer, const gls::GlsData *d) const
          {
          writer.StartObject();
          writer.Key("index");
          writer.Ubyte((uint8_t)(d->GambleNum));
          writer.Key("history");
          writer.StartArray();
          for (uint32_t i=0; i < gls::gls_gamble_history_size; ++i)
          writer.Ubyte(d->gambleHistory[i]);
          writer.EndArray();
          writer.Key("aiBetOn");
          writer.Uint(d->GambleRandom);
          writer.Key("lastWinCn");
          writer.Uint(d->GambleWin);
          writer.EndObject();
          }

          template<typename Writer>
          void Data2Json::serializeMeter(Writer &writer, const gls::GlsData *d) const
          {
          writer.StartObject();
          writer.Key("creditCn");
          writer.Uint64(d->TotalCents);
          writer.Key("winCn");
          writer.Uint("d->Win_cn");
          writer.Key("deno");
          writer.Uint("d->Denomination");
          writer.Key("baseCredit");
          writer.Uint("d->BaseCreditValue");
          writer.Key("betPerlineCn");
          writer.Uint("d->BetMultiplier");
          writer.Key("lines");
          writer.Uint("d->NumLineSelected");
          writer.EndObject();
          }

          template<typename Writer>
          void Data2Json::serializeFlag(Writer &writer, const gls::GlsData *d) const
          {
          uint8_t featureType = d->FG_type;
          uint8_t fakePlay = d->GameMode;
          uint8_t mainVolume = d->MainVolume;
          uint8_t betPerLineInc = d->allBetMultiplier[gls::gls_bet_per_line_opt_size-1] > d->BetMultiplier ? 1 : 0;
          uint8_t betPerLineDec = d->allBetMultiplier[0] < d->BetMultiplier ? 1 : 0;
          uint8_t lineInc = d->allNumLines[gls::gls_line_opt_size-1] > d->NumLineSelected ? 1 : 0;
          uint8_t lineDec = d->allNumLines[0] < d->NumLineSelected ? 1 : 0;
          uint8_t gamble = d->autoTakewin == 0 ? 1 : 0;
          uint8_t gameVersion = d->NormalDbgDemo;
          uint8_t reelStopSound = d->ReelStopSoundOn;

          writer.StartObject();
          writer.Key("featureType");
          writer.Ubyte(featureType);
          writer.Key("fakePlay");
          writer.Ubyte(fakePlay);
          writer.Key("mainVolume");
          writer.Ubyte(mainVolume);
          writer.Key("betPerLineInc");
          writer.Ubyte(betPerLineInc);
          writer.Key("betPerlineDec");
          writer.Ubyte(betPerLineDec);
          writer.Key("lineInc");
          writer.Ubyte(lineInc);
          writer.Key("lineDec");
          writer.Ubyte(lineDec);
          writer.Key("gamble");
          writer.Ubyte(gamble);
          writer.Key("gameVersion");
          writer.Ubyte(gameVersion);
          writer.Key("reelStopSound");
          writer.Ubyte(reelStopSound);
          writer.EndObject();
          }

          template<typename Writer>
          void Data2Json::serializeFreespin(Writer &writer, const gls::GlsData *data) const
          {
          }

          template<typename Writer>
          void Data2Json::serializeMessage(Writer &writer, const char *msg) const
          {
          writer.Key("message");
          writer.String(msg);
          }


          template<typename Writer>
          void Data2Json::serializeMessage(Writer &writer, const std::string *msg) const
          {
          writer.Key("message");
          writer.String(msg->c_str());
          }


          struct glsWinningLine{
          unsigned char sym[gls::gls_reel_size];
          unsigned char pos[gls::gls_reel_size];
          unsigned int winCn;
          unsigned int info;
          };

          struct glsRawWinningLine{
          unsigned char symPos[gls::gls_win_line_pattern_size];
          unsigned char winCn[gls::gls_win_line_pattern_size];
          };

          static void decodeWinLine(glsRawWinningLine *rl, glsWinningLine *wl)
          {

          for (uint32_t i=0, j=0; i<gls::gls_win_line_pattern_size;) {
          wl->sym[j] = rl->symPos[i];

          if (wl->sym[j] == '0') wl->sym[j] = 0xFF;
          else wl->sym[j] -= 'A';

          ++i;
          wl->pos[j] = rl->symPos[i];

          if (wl->pos[j] >= '0') wl->pos[j] -= '0';
          if (wl->pos[j]) --wl->pos[j];

          ++i;
          ++j;
          }
          char winCn[gls::gls_win_line_pattern_size + 1];
          memcpy(winCn, rl->winCn, gls::gls_win_line_pattern_size);
          winCn[gls::gls_win_line_pattern_size] = 0;
          wl->winCn = atol((const char*)winCn);
          wl->info = 0;
          }



          template<typename Writer>
          void Data2Json::serializeLineWin(Writer &writer, const gls::GlsData *d) const
          {
          writer.Key("lineWin");
          writer.StartArray();

          uint32_t winLineNum = 0;
          glsWinningLine wl;
          glsRawWinningLine *rl = (glsRawWinningLine *)(&d->WinWays);
          while (rl->symPos[0]) {
          decodeWinLine(rl, &wl);
          writer.StartObject();
          writer.Key("winCn");
          writer.Uint(wl.winCn);

          writer.Key("symbols");
          writer.StartArray();
          for (uint32_t i; i < gls::gls_reel_size; ++i)
          writer.Ubyte(wl.sym[i]);
          writer.EndArray();

          writer.Key("position");
          writer.StartArray();
          for (uint32_t i; i < gls::gls_reel_size; ++i)
          writer.Ubyte(wl.pos[i]);
          writer.EndArray();

          writer.EndObject();
          ++winLineNum;
          ++rl;
          }

          writer.EndArray();
          writer.Key("lineWinNum");
          writer.Uint(winLineNum);
          }
        */


        template<typename Writer>
        void serializeReelWin(Writer &writer, const SlotGameData& gameData, const SlotWinData& win)
        {
                writer.Key("winCn");
                writer.Uint(win.winCr);
                writer.Key("symbols");
                writer.StartArray();
                for(uint32_t i = 0; i < win.reels * win.rows; ++i) {
                        writer.Uint((uint32_t)gameData.symbols[i]);
                }
                writer.EndArray();
                if (win.feature.type) {
                        const TriggerData &feature = win.feature;
                        writer.Key("triggered");
                        writer.StartArray();
                        writer.StartObject();
                        writer.Key("type");
                        writer.Uint(feature.type);
                        writer.Key("chances");
                        writer.Uint(feature.chances);
                        writer.Key("multiplier");
                        writer.Uint(feature.multiplier);
                        writer.EndObject();
                        writer.EndArray();
                }
                if (win.lineWin.size()) {
                        const std::vector<LineWin> &lineWin = win.lineWin;
                        writer.Key("lineWin");
                        writer.StartArray();
                        for (auto lw = lineWin.begin(); lw != lineWin.end(); ++lw) {
                                writer.StartObject();
                                writer.Key("winCn");
                                writer.Uint(lw->winCr);
                                writer.Key("position");
                                writer.StartArray();
                                for(uint32_t i = 0; i < win.reels; ++i) {
                                        writer.Uint((uint32_t)lw->position[i]);
                                }
                                writer.EndArray();
                                writer.Key("winSymNum");
                                writer.String(std::string("L" + std::to_string(lw->winSymNum)).c_str());
                                writer.Key("winLineIdx");
                                writer.Uint(lw->lineIndex);
                                writer.EndObject();
                        }
                        writer.EndArray();
                }

                if (win.nonLineWin.size()) {
                        const std::vector<NonLineWin> &nonLineWin = win.nonLineWin;
                        writer.Key("nonLineWin");
                        writer.StartArray();
                        for (auto nlw = nonLineWin.begin(); nlw != nonLineWin.end(); ++nlw) {
                                writer.StartObject();
                                writer.Key("winCn");
                                writer.Uint(nlw->winCr);
                                writer.Key("symbols");
                                writer.StartArray();
                                for(uint32_t i = 0; i < win.reels * win.rows; ++i) {
                                        writer.Uint((uint32_t)nlw->symbols[i]);
                                }
                                writer.EndArray();
                                writer.Key("multiplier");
                                writer.Uint(nlw->multiplier);
                                writer.EndObject();
                        }
                        writer.EndArray();
                }
        }

        void serialize(const SlotGameData &gameData, const SlotWinData& win, std::string& buf)
        {
                uint32_t gameType = gameData.reelPaySet+1;

                StringBuffer s;
                PrettyWriter<StringBuffer> writer(s);
                writer.StartObject();

                switch (gameType) {
                case GT_NORMALSPIN:
                        serializeReelWin(writer, gameData, win);
                        break;
                case GT_FREESPIN:
                        serializeReelWin(writer, gameData, win);
                        writer.Key("chances");
                        writer.Uint(10);
                        writer.Key("multiplier");
                        writer.Uint(win.multiplier);
                        break;
                case GT_FREESPIN2:
                        break;
                case GT_FEATURE_BONUS:
                        break;
                case GT_FEATURE_BONUS2:
                        break;
                case GT_GAMBLE:
                        break;
                default:
                        break;
                }
                writer.EndObject();
                const char * ch = s.GetString();
                for(uint32_t i = 0; i < s.GetSize(); ++i) {
                        if (ch[i] != ' ' &&  ch[i] != '\n')
                                buf.push_back(ch[i]);
                }

        }

        void serialize(const std::vector<uint8_t>& screenSym, std::string& buf)
        {
                StringBuffer s;
                PrettyWriter<StringBuffer> writer(s);
                writer.StartObject();
                writer.Key("symbols");
                writer.StartArray();
                for(auto beg = screenSym.begin(); beg != screenSym.end(); ++beg) {
                        writer.Uint((uint32_t)*beg);
                }
                writer.EndArray();
                writer.EndObject();
                const char * ch = s.GetString();
                for(uint32_t i = 0; i < s.GetSize(); ++i) {
                        if (ch[i] != ' ' &&  ch[i] != '\n')
                                buf.push_back(ch[i]);
                }

        }

}
